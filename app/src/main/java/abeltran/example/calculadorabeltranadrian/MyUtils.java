package abeltran.example.calculadorabeltranadrian;

public class MyUtils {
    public static double calcularDivision(double n1, double n2){
        return n1/n2;
    }
    public static double calcularRaiz(double n1){
        return Math.sqrt(n1);
    }
}
