package abeltran.example.calculadorabeltranadrian;

        import androidx.appcompat.app.AppCompatActivity;

        import android.content.Intent;
        import android.os.Bundle;
        import android.view.View;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }
    public void actDivision(View view){
        Intent ActDivision = new Intent(this,Division.class);
        startActivity(ActDivision);
    }
    public void actRaiz(View view){
        Intent ActRaiz = new Intent(this,RaizCuadrada.class);
        startActivity(ActRaiz);
    }
}
