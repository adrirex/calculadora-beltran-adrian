package abeltran.example.calculadorabeltranadrian;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class Division extends AppCompatActivity {

    EditText txtn1;
    EditText txtn2;
    EditText txtresultado;

    private double resultado;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_division);

        txtresultado=(EditText)findViewById(R.id.txtresultado);

        if(savedInstanceState != null){
            resultado = savedInstanceState.getDouble("keyResultado");
            mostrarResultado();
        }

        Button dividir =(Button) findViewById(R.id.btnd);

        dividir.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                txtn1=(EditText)findViewById(R.id.txtn1);
                txtn2=(EditText)findViewById(R.id.txtn2);
                double n1=Integer.parseInt(txtn1.getText().toString());
                double n2=Integer.parseInt(txtn2.getText().toString());
                if (n1 < 0 || n2 < 0){
                    txtresultado.setText("ERROR: Dividendo y divisor no pueden ser negativos y deben ser números.");
                }else {
                    resultado = MyUtils.calcularDivision(n1,n2);
                    mostrarResultado();
                }
            }
        });
    }

    private void mostrarResultado(){
        txtresultado.setText(""+resultado);
    }

    @Override
    protected void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putDouble("keyResultado",resultado);
    }
}
