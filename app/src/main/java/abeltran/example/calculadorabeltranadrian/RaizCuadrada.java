package abeltran.example.calculadorabeltranadrian;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.os.PersistableBundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class RaizCuadrada extends AppCompatActivity {

    EditText txtn1;
    EditText txtresultado;
    private double resultado;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_raiz_cuadrada);

        txtresultado=findViewById(R.id.etSolucion);

        if(savedInstanceState != null){
            resultado = savedInstanceState.getDouble("keyResultado");
            mostrarResultado();
        }

        Button raiz =findViewById(R.id.btnraiz);

        raiz.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                txtn1=findViewById(R.id.etnumero);
                double n1=Integer.parseInt(txtn1.getText().toString());
                if (n1 < 0 ){
                    txtresultado.setText("ERROR: No puede ser negativos y deben ser números.");
                }else {
                    resultado=MyUtils.calcularRaiz(n1);
                    mostrarResultado();
                }
            }
        });
    }

    private void mostrarResultado(){
        txtresultado.setText(""+resultado);
    }

    @Override
    protected void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putDouble("keyResultado",resultado);
    }
}
